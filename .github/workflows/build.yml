name: Build geoip files
on:
  workflow_dispatch:
  schedule:
    - cron: "0 0 * * *"
  push:
    branches:
      - master
    paths-ignore:
      - ".gitignore"
      - "config-example.json"
      - "LICENSE"
      - "README.md"
      - ".github/dependabot.yml"
jobs:
  build:
    name: Build
    runs-on: ubuntu-latest
    steps:
      - name: Checkout codebase
        uses: actions/checkout@v4
        with:
          fetch-depth: 0

      - name: Set up Go
        uses: actions/setup-go@v5
        with:
          go-version: '1.22'

      - name: Set variables
        run: |
          echo "TAG_NAME=$(date +%Y%m%d%H%M)" >> $GITHUB_ENV
          echo "RELEASE_NAME=$(date +%Y%m%d%H%M)" >> $GITHUB_ENV
        shell: bash

      - name: Fetch lists from ripe.net
        run: |
          chmod +x asn.sh
          ./asn.sh
      
      - name: Fetch lists from APNIC
        run: |
          curl -sSL http://ftp.apnic.net/stats/apnic/delegated-apnic-latest > delegated-apnic-latest.list

      - name: Append more CIDRs
        run: |
          curl -sSL https://www.gstatic.com/ipranges/goog.json | jq --raw-output '.prefixes[].ipv4Prefix,.prefixes[].ipv6Prefix | select(. != null)' >> data/google
          curl -sSL https://www.gstatic.com/ipranges/cloud.json | jq --raw-output '.prefixes[].ipv4Prefix,.prefixes[].ipv6Prefix | select(. != null)' >> data/google
          curl -sSL https://api.fastly.com/public-ip-list | jq --raw-output '.addresses[],.ipv6_addresses[]' >> data/fastly
          curl -sSL https://ip-ranges.amazonaws.com/ip-ranges.json | jq --raw-output '.prefixes[],.ipv6_prefixes[] | select(.service == "CLOUDFRONT") | .ip_prefix,.ipv6_prefix' | grep "/" >> data/cloudfront
          curl -sSL http://ftp.apnic.net/stats/apnic/delegated-apnic-latest > ip.txt
          if [ "`cat ip.txt`" = "" ]; then curl -sSL https://raw.githubusercontent.com/CloudPassenger/geoip/release/ip.txt > ip.txt; fi   
          cat ip.txt | awk -F '|' '/CN/&&/ipv4/ {print $4 "/" 32-log($5)/log(2)}' > data/cn
          cat ip.txt | awk -F '|' '/CN/&&/ipv6/ {print $4 "/" $5}' >> data/cn
          curl -sSL https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/BiliBili/BiliBili_Resolve.list | grep IP-CIDR, | sed 's/IP-CIDR,//g' > data/bilibili
          curl -sSL https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/Apple/Apple_Resolve.list | grep IP-CIDR, | sed 's/IP-CIDR,//g' > data/apple
          curl -sSL https://raw.githubusercontent.com/blackmatrix7/ios_rule_script/master/rule/Loon/Whatsapp/Whatsapp_Resolve.list | grep IP-CIDR, | sed 's/IP-CIDR,//g' > data/whatsapp
          cat ip.txt | awk -F '|' '/JP/&&/ipv4/ {print $4 "/" 32-log($5)/log(2)}' > data/jp
          cat ip.txt | awk -F '|' '/JP/&&/ipv6/ {print $4 "/" $5}' >> data/jp
          cat ip.txt | awk -F '|' '/TW/&&/ipv4/ {print $4 "/" 32-log($5)/log(2)}' > data/tw
          cat ip.txt | awk -F '|' '/TW/&&/ipv6/ {print $4 "/" $5}' >> data/tw
          cat ip.txt | awk -F '|' '/HK/&&/ipv4/ {print $4 "/" 32-log($5)/log(2)}' > data/hk
          cat ip.txt | awk -F '|' '/HK/&&/ipv6/ {print $4 "/" $5}' >> data/hk
      
      - name: Get GeoLite2
        env:
          LICENSE_KEY: ${{ secrets.MAXMIND_GEOLITE2_LICENSE }}
        run: |
          curl -L "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country-CSV&license_key=${LICENSE_KEY}&suffix=zip" -o GeoLite2-Country-CSV.zip
          unzip GeoLite2-Country-CSV.zip
          rm -f GeoLite2-Country-CSV.zip
          mv GeoLite2* geolite2

      - name: Build geoip files
        run: |
          go run ./

      - name: Verify mmdb files
        run: |
          cd ./output/maxmind || exit 1
          go install -v github.com/maxmind/mmdbverify@latest
          for name in $(ls *.mmdb); do
            $(go env GOPATH)/bin/mmdbverify -file ${name}
          done

      - name: Build Sing-box and Clash.Meta Database
        env:
          NO_SKIP: true
        run: |
          mkdir -p ./output/meta
          go install -trimpath -ldflags="-s -w -buildid=" github.com/metacubex/geo/cmd/geo@master
          geo convert ip -i v2ray -o sing -f ./output/meta/geoip.db ./output/dat/geoip.dat
          geo convert ip -i v2ray -o meta -f ./output/meta/geoip.metadb ./output/dat/geoip.dat
          geo convert ip -i v2ray -o sing -f ./output/meta/geoip-mini.db ./output/dat/geoip-mini.dat
          geo convert ip -i v2ray -o meta -f ./output/meta/geoip-mini.metadb ./output/dat/geoip-mini.dat
          geo convert ip -i v2ray -o sing -f ./output/meta/geoip-only-cn-private.db ./output/dat/geoip-only-cn-private.dat
          geo convert ip -i v2ray -o meta -f ./output/meta/geoip-only-cn-private.metadb ./output/dat/geoip-only-cn-private.dat

      - name: Generate sha256 checksum for dat files
        run: |
          cd ./output/dat || exit 1
          for name in $(ls *.dat); do
            sha256sum ${name} > ./${name}.sha256sum
          done

      - name: Generate sha256 checksum for mmdb files
        run: |
          cd ./output/maxmind || exit 1
          for name in $(ls *.mmdb); do
            sha256sum ${name} > ./${name}.sha256sum
          done
      - name: Generate sha256 checksum for db & metadb files
        run: |
          cd ./output/meta || exit 1
          for name in $(ls *.db); do
            sha256sum ${name} > ./${name}.sha256sum
          done
          for name in $(ls *.metadb); do
            sha256sum ${name} > ./${name}.sha256sum
          done

      - name: Move files to publish directory
        run: |
          mkdir -p publish
          mv ./output/dat/*.dat ./output/dat/*.sha256sum ./output/maxmind/*.mmdb ./output/maxmind/*.sha256sum ./publish/
          cp ./data/cn ./publish/cn.txt
          cp ./ip.txt ./publish/ip.txt
          cp -fpPR ./output/text ./publish

      - name: Git push assets to "release" branch
        run: |
          cd publish || exit 1
          git init
          git config --local user.name "github-actions[bot]"
          git config --local user.email "41898282+github-actions[bot]@users.noreply.github.com"
          git checkout -b release
          git add -A
          git commit -m "${{ env.RELEASE_NAME }}"
          git remote add geoip "https://${{ github.actor }}:${{ secrets.GITHUB_TOKEN }}@github.com/${{ github.repository }}"
          git push -f -u geoip release

      - name: Purge jsdelivr CDN
        run: |
          cd publish || exit 1
          for file in $(ls); do
            curl -i "https://purge.jsdelivr.net/gh/${{ github.repository }}@release/${file}"
          done

      - name: Remove some files to avoid publishing to GitHub release
        run: rm -rf ./publish/*.{gz,zip} ./publish/text

      - name: Upload files to GitHub release
        uses: softprops/action-gh-release@v1
        with:
          name: latest
          tag_name: latest
          draft: false
          prerelease: false
          files: |
            ./publish/*
        env:
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}